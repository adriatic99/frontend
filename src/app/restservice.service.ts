import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestserviceService {

  private a1testerandanalyzerUrl = "http://localhost:7100/testdataapi";

  constructor(private http: HttpClient) {
    this.getData();
   }

   getJobs(): Observable<String[]> {
    return this.http.get<String[]>(this.a1testerandanalyzerUrl);
  }

  /** GET heroes from the server */
  getData() {
    console.log("getData() service");
    const headers = new HttpHeaders({authorization: 'Basic ' + 'user:29e5c191-557c-4981-9ead-6715b0b63884'});
    return this.http.get(this.a1testerandanalyzerUrl, {headers:headers});
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
