import { Component } from '@angular/core';
import { RestserviceService } from './restservice.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'a1webshoptesteranalyzer';
  products: String[] = [];

  constructor(private comp: RestserviceService) {
      this.comp.getJobs().subscribe((res : String[]) => {
        this.products =res;
      });
  }
}
