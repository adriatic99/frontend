FROM node:14.1-alpine AS builder
# set working directory
WORKDIR /app
RUN npm link @angular/cli
COPY . .
RUN npm i && npm run build

# start app
EXPOSE 4200
CMD ng serve --host 0.0.0.0